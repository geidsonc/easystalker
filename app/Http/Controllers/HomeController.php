<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Contact;
use App\Http\Requests;

class HomeController extends Controller
{
    public function anyIndex(Request $in) {
      return view("index");
    }

    public function anySaveEmail(Request $in) {
      $contato = new Contact;
      $contato->email = $in->email;
      $contato->save();
      return response()->json(['status' => '1']);
    }

    public function anyGetEmails(Request $in) {
      return response()->json(['emails' => Contact::all()]);
    }
}
