<!DOCTYPE html>

<html lang="en">
<head>

<!-- Html Page Specific -->
<meta charset="utf-8">
<title>EasyTalker</title>
<meta name="description" content="EasyStalker">

<!-- Mobile Specific -->
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">

<!--[if lt IE 9]>
    <script type="text/javascript" src="scripts/html5shiv.js"></script>
<![endif]-->

<!-- CSS -->
<link rel="stylesheet" href="css/bootstrap.min.css"/>
<link rel="stylesheet" href="css/animate.css"/>
<link rel="stylesheet" href="css/simple-line-icons.css"/>
<link rel="stylesheet" href="css/icomoon-soc-icons.css"/>
<link rel="stylesheet" href="css/magnific-popup.css"/>
<link rel="stylesheet" href="css/style.css"/>

<!-- Favicons -->
<!-- <link rel="icon" href="images/favicon.png">
<link rel="apple-touch-icon" href="images/apple-touch-icon.png">
<link rel="apple-touch-icon" sizes="72x72" href="images/apple-touch-icon-72x72.png">
<link rel="apple-touch-icon" sizes="114x114" href="images/apple-touch-icon-114x114.png"> -->
<style>
	.white {
		color: #fff;
	}
</style>
</head>

<body data-spy="scroll" data-target=".navMenuCollapse">

<!-- PRELOADER -->
<div id="preloader">
	<div class="battery inner">
		<div class="load-line"></div>
	</div>
</div>

<div id="wrap">

	<!-- NAVIGATION BEGIN -->
	<nav class="navbar navbar-fixed-top navbar-slide" style="background-color: #206bab">
			<div class="container_fluid">
				<a class="navbar-brand goto" href="index.html#wrap"> <img src="./images/easystalker.png" alt="Your logo" height="27" width="120" /> </a>
				<!-- <a class="contact-btn icon-envelope" data-toggle="modal" data-target="#modalContact"></a> -->
				<button class="navbar-toggle menu-collapse-btn collapsed" data-toggle="collapse" data-target=".navMenuCollapse"> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
				<div class="collapse navbar-collapse navMenuCollapse">
					<ul class="nav white">
						<li><a href="#features">Funcionalidades</a> </li>
						<li><a href="#benefits1">Benefícios</a></li>
						<li><a href="#screenshots">Screenshots</a></li>
						<li><a href="#pricing-table">Planos</a></li>
					</ul>
				</div>
			</div>
	</nav>
	<!-- NAVIGAION END -->



	<!-- INTRO BEGIN -->
	<header id="full-intro" class="intro-block bg-color-main" >
		<div class="ray ray-vertical y-100 x-50 ray-rotate-135 laser-blink hidden-sm hidden-xs" ></div>
		<div class="ray ray-horizontal y-25 x-0 ray-rotate-45 laser-blink hidden-sm hidden-xs" ></div>
		<div class="container">
			<div class="row">
				<div class="col-md-8 col-sm-12">
					<img class="logo" src="./images/easystalker.png" alt="Your Logo" width="80%" />
					<h1 class="slogan">Veja tudo de forma <br> simples e rápida</h1>
					<img src="images/socials.png" width="60%" >
					<!-- <a class="download-btn-alt ios-btn" href="#">
						<i class="icon soc-icon-apple"></i>Download for <b>Apple iOS</b>
					</a> -->
				</div>
				<div class="col-md-4 hidden-sm hidden-xs">
					<img class="intro-screen wow bounceInUp" data-wow-delay="0.5s" src="images/screen1.png" width="440" height="775" alt="" />
				</div>
			</div>
		</div>
		<div class="block-bg" data-stellar-ratio="0.4"></div>
	</header>
	<!-- INTRO END -->



	<section id="features" class="img-block-2col">
		<div class="container">
			<div class="row">
				<div class="col-sm-6">
					<div class="title">
						<h2>Funcionalidades</h2>
					</div>

					<ul class="item-list-left">
						<li>
							<i class="icon icon-picture color"></i>
							<h4 class="color">Dados pontuais</h4>
							<p>Interesses pessoais e interações nas redes sociais</p>
						</li>
						<li>
							<i class="icon icon-equalizer color"></i>
							<h4 class="color">Histórico</h4>
							<p>Log online</p>
							<p>Log de curtidas e conexões</p>
							<p>Localizações</p>
						</li>
						<li>
							<i class="icon icon-bar-chart color"></i>
							<h4 class="color">Relatórios</h4>
							<p>Visibilidade diária, semanal e mensal</p>
						</li>
					</ul>
				</div>
				<div class="col-md-5 col-md-offset-1 col-sm-6">
					<div class="screen-couple-right wow fadeInRightBig">
						<div class="flare">
							<img class="base wow" src="images/flare_base.png" alt="" />
							<img class="shapes wow" src="images/flare_shapes.png" alt="" />
						</div>
						<img class="screen above" src="images/screen2.png" alt="" height="484" width="250" />
						<img class="screen beyond wow fadeInRight" data-wow-delay="0.5s" src="images/screen3.png" alt="" height="407" width="210" />
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- BENEFITS1 END -->

	<!-- FEATURES BEGIN -->
	<section id="benefits1" class="img-block-3col bg-color2">
		<div class="container">
			<div class="title">
				<h2>Benefícios</h2>
				<p>Para quem serve nosso aplicativo:</p>
			</div>
			<div class="row">
				<div class="col-sm-4">
					<br>
					<br>
					<br>
					<br>
					<br>
					<ul class="item-list-right item-list-big">
						<li class="wow fadeInLeft">
							<!-- <i class="icon icon-screen-desktop"></i> -->
							<h3>Relacionamentos</h3>
							<p>Descubra toda a verdade</p>
						</li>
						<br>
						<br>
						<br>
						<li class="wow fadeInLeft">
							<!-- <i class="icon icon-drop"></i> -->
							<h3>Monitoramento Parental</h3>
							<p>Segurança e monitoramento das atividades virtuais</p>
						</li>
					</ul>
				</div>
				<div class="col-sm-4 col-sm-push-4">
					<br>
					<br>
					<br>
					<br>
					<br>
					<ul class="item-list-left item-list-big">
						<li class="wow fadeInRight">
							<!-- <i class="icon icon-diamond"></i> -->
							<h3>Comercial</h3>
							<p>Interesses pessoais de potenciais clientes e sócios</p>
							<p>Comportamento dos seus funcionários</p>
						</li>
						<li class="wow fadeInRight">
							<!-- <i class="icon icon-layers"></i> -->
							<h3>Ídolos</h3>
							<p>Descubra os hábitos e interesses dos seus ídolos</p>
						</li>
					</ul>
				</div>
				<div class="col-sm-4 col-sm-pull-4">
					<div class="animation-box wow bounceIn">
					 	<img class="highlight-left wow" src="images/light.png" height="192" width="48" alt="" />
						<img class="highlight-right wow" src="images/light.png" height="192" width="48" alt="" />
						<img class="screen" src="images/screen1.png" alt="" height="581" width="350" />
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- FEATURES END -->

	<!-- VIDEO BEGIN -->
	<!-- <section id="video">
		<div class="container">
			<div class="title">
				<h2>Iframe Video</h2>
				<p>In our work we try to use only the most modern, convenient and interesting solutions.</p>
			</div>
			<div class="video-container">
				<!-- Video iframe code here -->
			</div>
		</div>
	</section> -->
	<!-- VIDEO END -->

	<!-- SCREENSHOTS BEGIN -->
	<section id="screenshots" class="">
		<div class="container-fluid wow fadeIn">
			<h2>Screenshots</h2>
			<div id="screenshots-slider" class="owl-carousel">
				<a class="item" href="./images/shot1.png" title="App Screen 1"><img src="images/shot1.png" alt="screen1" width="250" height="444" /></a>
				<a class="item" href="./images/shot2.png" title="App Screen 2"><img src="images/shot2.png" alt="screen1" width="250" height="444"/></a>
				<a class="item" href="./images/shot3.png" title="App Screen 3"><img src="images/shot3.png" alt="screen1" width="250" height="444"/></a>
				<a class="item" href="./images/shot4.png" title="App Screen 4"><img src="images/shot4.png" alt="screen1" width="250" height="444"/></a>
			</div>
		</div>
	</section>
	<!-- SCREENSHOTS END -->



	<!-- FACTS BEGIN -->
	<!-- <section id="facts">
		<div class="container">
			<ul class="facts-list">
				<li class="wow bounceIn">
					<i class="icon icon-cloud-download"></i>
					<h3 class="wow">284</h3>
					<h4>Downloads</h4>
				</li>
				<li class="wow bounceIn" data-wow-delay="0.4s">
					<i class="icon icon-star"></i>
					<h3 class="wow">135</h3>
					<h4>Top Rates</h4>
				</li>
				<li class="wow bounceIn" data-wow-delay="0.8s">
					<i class="icon icon-like"></i>
					<h3 class="wow">77</h3>
					<h4>Likes</h4>
				</li>
				<li class="wow bounceIn" data-wow-delay="1.2s">
					<i class="icon icon-clock"></i>
					<h3 class="wow">741</h3>
					<h4>Hours Safe</h4>
				</li>
			</ul>
		</div>
	</section> -->
	<!-- FACTS END -->

	<!-- PRICING TABLE BEGIN -->
	<section id="pricing-table" class="bg-color-grad">
		<div class="container">
			<div class="title text-center">
				<h2>Planos</h2>
				<ul class="pricing-table">
					<li class="wow flipInY">
						<h3>Standard</h3>
						<span> Grátis <small>por um dia</small></span>
						<ul class="benefits-list">
							<li>1 dia de acesso limitado</li>
							<li>Interesses (dados pontuais)</li>
							<li>Log de curtidas/conexões</li>
							<li>Log online</li>
							<li>Localizações</li>
							<li class="not">Relatórios</li>
						</ul>
						<a href="#" class="buy"> <i class="icon icon-basket" ></i></a>
					</li>
					<!-- <li class="silver wow flipInY" data-wow-delay="0.2s">
						<h3>Sliver</h3>
						<span> $3.99 <small>per month</small> </span>
						<ul class="benefits-list">
							<li>Responsive</li>
							<li>Documentation</li>
							<li>Multiplatform</li>
							<li class="not">Video background</li>
							<li class="not">Support</li>
						</ul>
						<a href="#" class="buy"> <i class="icon icon-basket" ></i></a>
					</li> -->
					<li class="gold wow flipInY" data-wow-delay="0.4s">
						<!-- <div class="stamp"><i class="icon icon-trophy"></i>Best choice</div> -->
						<h3>Gold</h3>
						<span> R$5,99<small>por mês</small> </span>
						<ul class="benefits-list">
							<li>30 dias de acesso ilimitado</li>
							<li>Interesses (dados pontuais)</li>
							<li>Log de curtidas/conexões</li>
							<li>Log online</li>
							<li>Localizações</li>
							<li>Relatórios</li>
						</ul>
						<a href="#" class="buy"> <i class="icon icon-basket" ></i></a>
					</li>
					<!-- <li class="platinum wow flipInY" data-wow-delay="0.6s">
						<h3>Platinum</h3>
						<span> $12.99 <small>per month</small> </span>
						<ul class="benefits-list">
							<li>Responsive</li>
							<li>Documentation</li>
							<li>Multiplatform</li>
							<li>Video background</li>
							<li>Support</li>
						</ul>
						<a href="#" class="buy"> <i class="icon icon-basket" ></i></a>
					</li> -->
				</ul>
			</div>
		</div>
	</section>
	<!-- PRICING TABLE END -->

<section id="social" class="bg-color2">
		<div class="container-fluid">
			<div class="title">
				<h2>Entre em contato</h2>
				<p>Envie seu email para saber mais informações.</p>
			</div>

			<form id="subscribe_form">
            	<div class="input-group">
                	<input class="form-control" type="email" name="email" id="subscribe_email" placeholder="Informe seu email.">
                    <div class="input-group-btn">
                    	<button type="submit" <i class="icon icon-paper-plane"></i></button>
                    </div>
                </div>
            </form>

		</div>
	</section> --> -->
	<!-- SOCIAL END -->

	<!-- FOOTER BEGIN -->
	<footer id="footer">
		<div class="container">
			<a href="index.html#wrap" class="logo goto"> <img src="./images/easystalker.png" alt="Your Logo" height="35" width="160"/> </a>
			<p class="copyright">&copy; 2016 EsayStalker <br>
			<!-- Designed by <a href="http://multifour.com/" target="_blank">Multifour.com</a></p> -->
		</div>
	</footer>
	<!-- FOOTER END -->

</div>


<!-- MODALS BEGIN-->

<!-- subscribe modal-->
<div class="modal fade" id="modalMessage" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			<h3 class="modal-title"></h3>
		</div>
	</div>
</div>

<!-- contact modal-->
<div class="modal fade" id="modalContact" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			<h3 class="modal-title">Contact</h3>
			<form action="scripts/contact.php" role="form"  id="contact_form">
						<div class="form-group">
							<input type="text" class="form-control" id="contact_name" placeholder="Full name" name="name">
						</div>
						<div class="form-group">
							<input type="email" class="form-control" id="contact_email" placeholder="Email Address" name="email">
						</div>
						<div class="form-group">
							<textarea class="form-control" rows="3" placeholder="Your message or question" id="contact_message" name="message"></textarea>
						</div>
						<button type="submit" id="contact_submit" data-loading-text="&bull;&bull;&bull;"> <i class="icon icon-paper-plane"></i></button>
					</form>
		</div>
	</div>
</div>

<!-- MODALS END-->


<!-- JavaScript -->
<script src="scripts/jquery-1.8.2.min.js"></script>
<script src="scripts/bootstrap.min.js"></script>
<script src="scripts/owl.carousel.min.js"></script>
<script src="scripts/jquery.validate.min.js"></script>
<script src="scripts/wow.min.js"></script>
<script src="scripts/smoothscroll.js"></script>
<script src="scripts/jquery.smooth-scroll.min.js"></script>
<script src="scripts/jquery.superslides.min.js"></script>
<script src="scripts/placeholders.jquery.min.js"></script>
<script src="scripts/jquery.magnific-popup.min.js"></script>
<script src="scripts/jquery.stellar.min.js"></script>
<script src="scripts/retina.min.js"></script>
<script src="scripts/custom.js"></script>

<!--[if lte IE 9]>
	<script src="scripts/respond.min.js"></script>
<![endif]-->
</body>
</html>
